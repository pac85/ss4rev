# Intro

Out of curiosity I used RenderDoc to dig a bit into Serious Sam 4.

The first frame we are going to dissect is the following:
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/bloomfinal.png?inline=false)

# Shadow Maps

The game starts by rendering the shadow maps. Only the sun has shadows,
4 Cascades are rendered to separate textures, the size goes from 2048 to 5120 depending on the settings.

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/before/shadowmap1.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/before/shadowmap2.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/before/shadowmap3.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/before/shadowmap4.png?inline=false)

Whether the terrain is rendered or not also depends on the settings.

# Pre pass

the first pass on the scene has two render targets. After every opqaue element of the scene is drawn they look like this:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/before/predepth.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/before/preredh.png?inline=false)

On lower settings one of the buffers looks like this:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/before/prered.png?inline=false)

So one of the is clearly the depth buffer. I'm not sure about the other one.

We sill see how the various elements are drawn later.

# Light pass

For each light in the scene a drawcall is made. The drawcall draws a sphere (made of 960 vertices) around the light, in the pixel the actual light calculation is performed and stored in a RGB16 hdr buffer. The only input to those two buffers are the two buffers generated in the pre pass. Normals are probably reconstructed from depth, materials attributes and normal maps have therefore no way of influencing this pass.

## Final image:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/lightpass/hdr.png?inline=false)

## Sphere for green light

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/lightpass/lightmesh960.png?inline=false)

# Main scene pass

During this pass all of the elements from the first pass are redrawn plus transparent objects and particles.

Not all of them are that interesting, let's talk about the most interesting ones.

## Background bridge

Instanced rendering is used to draw the various sections

## Terrain

[Those slides](https://www.gdcvault.com/play/1026349/Advanced-Graphics-Techniques-Tutorial-Four) go into some detail on how the terrain system works.
In any case the terrain is drawn in chunks

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/before/terrain.png?inline=false)

A vertex buffer contains the following mesh

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/before/terrainin.png?inline=false)

which the vertex shader transforms in the fhe final terrain mesh for the current chunk

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/before/terrainout.png?inline=false)

Chunk size can vary based on distance and required resolution.

## Legion system

Finally let's dig right in to the legion system.

Closer enemies and NPCs are rendered normaly in several drawcalls:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/legionhalf1mesh.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/legionhalf1.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/legionhalf2mesh.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/legionhlaf2.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/legiongunmesh58341.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/legiongun.png?inline=false)

Interestingly the gun for this NPC has 58341 triangles despite only covering a couple tens of pixels.

Next, for far away enemies an impostor mesh is generated, containing a quad for each enemy.

![](https://gitlab.com/pac85/ss4rev/-/raw/master/legionquads.png?inline=false)

Their textures are stored in an atlas that seems to contain animations frames as well as various orientations.

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/legionaltlas.png?inline=false)

In just one drawcall tens of  thousand of distant enemies are drawn. The process os repeated for different types of enemies.

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/legioncolor.png?inline=false)

The vertex buffer is probably uploaded from the cpu every frame.

## Sky

For the sky a cube is rendered in 6 separate drawcalls (one for each side)

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/sky1.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/sky2.png?inline=false)

The sky itself is stored in 5 2d textures.

## Portal

First a textured quad with alpha blending is rendered

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/skysmokebeforeplasma.png?inline=false)

Then a bumpy circle mesh is rendered multiple times with different textures

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/plasmamesh5946.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/plasmameshside.png?inline=false)

In 3 passes the portal effect is rendered:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/plasmasrc.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/plasma1.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/galaxysrc.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/galaxy1.png?inline=false)

This one does distorsion

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/distnorm.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/plasmadist.png?inline=false)

## Particles

Particles are rendered in a single drawcall.
I suppose the vertex buffers and particle simulations are done in the cpu.

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/magictrail.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/magictrail-mesh.png?inline=false)

# Bloom

At this point, in order to create a bloom effect, the frame buffer is downcaled and upscaled multiple times.
During the first upscale dark parts of the buffer are rendered as black.

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/bloom816x459.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/bloom408x230.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/bloom204x115.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/bloom102x58.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/bloom51x29.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/bloom26x15.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/bloom13x8.png?inline=false)
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/bloom7x4.png?inline=false)

Various upscaling passes are omitted. At the end a 408x230 image is obtained

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/bloomup408x230.png?inline=false)

# Mixing and tone mapping

So far the image has been renderd into an hdr render target.
This pass does tone mapping and mixes the result with the bloom buffer

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/bloomfinal.png?inline=false)

# FXAA

before fxaa

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/aa1632x918.png?inline=false)

after
![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/aag.png?inline=false)

# UI

At this point UI is rendered on top of the LDR buffer and we get the final frame

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/ui.png?inline=false)

# Lighting

During the second pass the pixel shader calculates all of the lighting.
Let's see how it works by dissecting a different frame:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/final.png?inline=false)

# Terrain patch
To be precise we are going to look at the shader inputs for one of the terrain patches:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/terrainandmodels.png?inline=false)

Some of the shader inputs are buffers or black images, but some have a much clearer purpose:
This one looks like albedo

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/mdin1.png?inline=false)

Some kind of normal map

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/mdin2.png?inline=false)

One of the faces of a cubemap (used for specular reflections)

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/mdin3.png?inline=false)

A textures that seems to contain a light map

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/mdin4.png?inline=false)

This is the lightmap for a different model

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/mdin4b.png?inline=false)

Sun shadow map

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/smokeshadowmap.png?inline=false)

# Gun
Let's also see the shader inputs for the gun

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/gun1.png?inline=false)

Cubemap face

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/gun2.png?inline=false)

Normal map

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/gun3.png?inline=false)

Albedo

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/gun4.png?inline=false)

Not sure about this one, probably contains several material attributes in various chanels

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/gun5.png?inline=false)

Sun shadow map

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/smokeshadowmap.png?inline=false)

# Smoke 

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/smoke.png?inline=false)

The smoke is rendered as particles. They work like the ones we've seen before but those are lit.
The shader has (among other things) those two images as input.

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/smokeatlas.png?inline=false)

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ssframetwo/smokeshadowmap.png?inline=false)

# Conclusion

The game doesn't make use of many compute shaders, instead many passes just render a quad to fill the entire screen. 
Some compute shaders are used at the very beginning for a purpose that I haven't managed to figure out.
Overall the rendering is mainly done like in previous games of the series and has nothing special but despite that
the overall look is pleasing to the eye.
Despite the simplicity of the rendering pipeline the game requires a pretty good PC to run properly.
