# Part two
I did some more digging and found a few more things:

## Explaining the colorfull buffer
I'm talking about his one to be precise:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/ss4pics/before/preredh.png?inline=false)

After having a look at the shader's assembly code it turns out it encodes depth. I actually suspected that but I wasn't too sure. Still no clue on why it's done this way.

## SSAO

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2/ao.png?inline=false)

SSAO is stored in the alpha channel of an RGBA8 texture. The RGB part is white everywhere except for the sky which is black.
It is done by a pixel shader invocated on a screen filling quad. inputs to it are depth and a noise texture.
Somewhere it gets filtered and then mixed in in the second scene pass.

## Weird point light
Point lights in this game are usually fairly limited but this one was doing a bit more than usual:
First it gets rendered into the light buffers just like any other lamp. The intensity of the light is varied according to a cube map, that's why it seems like the barrel casts a shadow, in reality it does not. Earlier titles of the series had that too.

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2/pointshadow1.png?inline=false)


### Shadows

Now, during the second pass on the scene, this model get's rendered:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2/pointshadowmesh.png?inline=false)

Yes, that's a piece of geometry from the level...
A depth cube map is given as an input, one of the faces looks like this:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2/pointcubemap.png?inline=false)

And this is what gets rendered in the hdr buffer:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2/pointshadowfinal.png?inline=false)

Now that's weird, why aren't shadows calculated in the first light pass? Is the same shadow map applied to all lights? 
Is the light buffer ignored for this model? does it substract the light contribution on shadowed parts? I don't know but perhaps I might look into the shader and update this post.

### Floor 

You  might have noticed that the floor hasn't been rendered yet. Well not to worry because it's goin to be rendered not once but three times now.
I can't tell the difference between the first two passes, they both look like this:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2/floor1.png?inline=false)

(notice that contribution from other point lights is taken into account)
But on the third pass specular light from that light source is added

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2/floor3.png?inline=false)

## Volumetrics
At this point this geometry get's drawn:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2/godraysmesh.png?inline=false)

With this texture as an input to the pixel shader:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2/godraysin.png?inline=false)

The result isn't too bad:

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2/godraysout.png?inline=false)

# Update
Thanks to a user who created a custom map for me I was able to better understand how the different types of point lights are handled.

There are mainly two types:
* Dynamic omni lights
* Fast lights

The custom map is divided in two rooms with each containing two lights per type

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2up/final.png?inline=false)

## Fast lights

First they are rendered using the usual sphere 

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2up/fast.png?inline=false)

Normal maps aren't taken into account

After that a fragment shader ran on a quad filters the ssao buffer and stores the result in the alpha channel of a texture that otherwise looks blue.

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2up/ssao_filtered.png?inline=false)

That alpha channel gets copied (using a fragment shader on a quad) to the alpha channel of the light buffer.

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2up/lights_ssao.png?inline=false)

## Scene pass

Now I've chosen a part of the scene geometry that is lit by all lights.

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2up/scenemesh.png?inline=false)

It is rendered two times, first the contribution of the fast lights plus one of the dynamic lights is written to the hdr buffer.
![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2up/scene1.png?inline=false)

Notice the shadow in the back of the short hallway.
The alpha channel of the lights buffer is multiplied with the other channels.
Essentially this applies SSAO to the lighting.

It is then rendered once again in order to add the second dynamic light's contribution

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2up/scene2.png?inline=false)

Notice there are two shadows now.

Normals are taken into account for dynamic lights

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2up/normals.png?inline=false)

But since fast lights are calculated before the normal map is ever read from the material, it isn't taken into account for them

![](https://gitlab.com/pac85/ss4rev/-/raw/master/part2up/nonormals.png?inline=false)
